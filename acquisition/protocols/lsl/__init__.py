"""Binary packet definitions for various EEG Data streaming devices."""

from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import sys
from os.path import dirname
sys.path.append(dirname(__file__))
sys.path.append('.')
sys.path.append('..')
